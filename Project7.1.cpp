﻿// Project7.1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;

int main()
{
    double y;
    double x;

    for (x = -1; x <= 1; x = x + 0.6)
    {
        y = 6 * pow(x, 8) + 10 * (x - 21);
        cout << " x: " << x << " y: " << y << endl;
    }
    return 0;
}
